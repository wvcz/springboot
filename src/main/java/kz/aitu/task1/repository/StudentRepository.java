package kz.aitu.task1.repository;


import kz.aitu.task1.model.Student;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


import java.util.List;
@Repository
public interface StudentRepository extends CrudRepository<Student, Long> {

    @Query(value = "select * from student order by groupid", nativeQuery = true)
    List<Student> orderAllByGroupId();
    @Query(value = "select * from student where groupid = ?", nativeQuery = true)
    List<Student> findAllByGroupId(long groupid);


}
