package kz.aitu.task1.model;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

import static javax.persistence.GenerationType.IDENTITY;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table
public class Student {


    @Id
    @GeneratedValue(strategy = IDENTITY)
    private int studentid;

    @Column
    private String studentname;
    private String phone;
    private int groupid;



}