package kz.aitu.task1.controller;

import kz.aitu.task1.repository.StudentRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class StudentController {

    private final StudentRepository studentRepository;


    public StudentController(StudentRepository studentRepository) {
        this.studentRepository = studentRepository;
    }
    @GetMapping("/student/{groupid}")
    public ResponseEntity<?> getStudentsById(@PathVariable long groupid) {
        return ResponseEntity.ok(studentRepository.findAllByGroupId(groupid));
    }
    @GetMapping("/student")
    public ResponseEntity<?> getStudents() {
        return ResponseEntity.ok(studentRepository.orderAllByGroupId());
    }


}

